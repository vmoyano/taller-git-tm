
public class TestOrdenamiento {

	static void mostrar(int[] x) {
		System.out.print("[ ");
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println("]");
	}

	static void ordenarPorSeleccion(int a[]) {
		for (int i = 0; i < a.length - 1; i++) {
			int menor = i;
			for (int j = i + 1; j < a.length; j++) {
				if (a[j] < a[menor])
					menor = j;
			}

			swap(a, i, menor);
		}
	}

	static void ordenarPorInsercion(int a[]) {
		for (int i = 1; i < a.length; i++) {
			int pos = 0;
			while (pos < i && a[pos] < a[i]) {
				pos++;
			}
			int temp = a[i];
			for (int j = i - 1; j >= pos; j--) {
				a[j + 1] = a[j];
			}
			a[pos] = temp;
		}
	}

	static void swap(int[] a, int x, int y) {
		int temp = a[x];
		a[x] = a[y];
		a[y] = temp;
	}

	static void ordenarPorBurbujeo(int a[]) {
		for (int i = a.length - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (a[j] > a[j + 1]) {
					swap(a, j, j + 1);
				}
			}
		}
	}

	public static void main(String[] args) {
		int[] a = { 5, 4, 22, 7, 8, 1, 15, 30, 2 };
		mostrar(a);
		System.out.println("Por seleccion: ");
		ordenarPorSeleccion(a);
		mostrar(a);
		int[] b = { 5, 4, 22, 7, 8, 1, 15, 30, 2 };
		System.out.println("Por insercion: ");
		ordenarPorInsercion(b);
		mostrar(b);
		int[] c = { 5, 4, 22, 7, 8, 1, 15, 30, 2 };
		System.out.println("Por burbujeo: ");
		ordenarPorBurbujeo(c);
		mostrar(c);

	}

}
